<?php
require_once(__DIR__.'/../../config.php');

$id = $_GET['userid'];
$filea = $_GET['filearea'];
$itemi = $_GET['itemid'];

if($itemi == null){
  echo "Best solution is yet to discover....!";
}
else{
//function display_file(){
  global $DB;
  $files_result = $DB->get_record('files', array('userid' => $id, 'filearea' => 'submission_files', 'itemid' => $itemi));
  $fs = get_file_storage();

  // Prepare file record object
  $fileinfo = array(
  'component' => $files_result->component,     // usually = table name
  'filearea' => $files_result->filearea,     // usually = table name
  'itemid' => $files_result->itemid,               // usually = ID of row in table
  'contextid' => $files_result->contextid, // ID of context
  'filepath' => $files_result->filepath,           // any path beginning and ending in /
  'filename' => $files_result->filename); // any filename

  // Get file
  $file = $fs->get_file($fileinfo['contextid'], $fileinfo['component'], $fileinfo['filearea'],
                    $fileinfo['itemid'], $fileinfo['filepath'], $fileinfo['filename']);

  if ($file) {
    $contents = $file->get_content();
    echo "<br/><br/>Best solution is here : <br/><br/>";
    echo $contents;
  } else {
    echo "Something is wrong with uploaded file.";
  }
}

?>
