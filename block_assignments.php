<?php
defined('MOODLE_INTERNAL') || die;
require_once($CFG->libdir . '/gradelib.php');
require_once($CFG->libdir . '/enrollib.php');
require_once($CFG->dirroot . '/mod/assign/lib.php');
require_once($CFG->dirroot . '/lib/grade/grade_item.php');
require_once("{$CFG->libdir}/formslib.php");
//require_once($CFG->libdir . '/pagelib.php');

//echo "<script src=assignments.js> </script>";
class block_assignments extends block_base{
  function init(){
    $this->title = get_string('pluginname','block_assignments');
  }
  public function get_content(){
    global $DB, $CFG;
    if($this->content !== null){
      return $this->content;
    }
    $id = required_param('id', PARAM_INT);
    $context = context_course::instance($id);

    $allowedusers = get_enrolled_users($context, '', 0, 'u.*', '',0,0);

    $u = array_keys($allowedusers);
    //get grade item information
    $grade_item = $DB->get_records('grade_items', array('courseid' => $id, 'itemtype' => 'mod', 'itemmodule' => 'assign'));
  //  print_r($grade_item);
    $grade_item_count = count($grade_item);
    $item_keys = array_keys($grade_item);

    $real_assign_id = array();
    $real_assign_id = $DB->get_records('assign', array('course' => $id));

    //retrieve grades
    $this->content = new stdClass();
    //retrive grade information for each instance
    for($k = 1; $k <= $grade_item_count; $k++){

      $result = grade_get_grades($id, 'mod', 'assign', $k, $u);

      if($result == null){
        $this->content->text = 'result null';
      }
      else{
        $grades_array_count = count($result->items[0]->grades);
          $highest_graded_user = $u[0];
          $highest_grade = 0;

        $content_string .= "Grades for assignment : ".$grade_item[$result->items[0]->id]->itemname;
        for($i = 0; $i < count($allowedusers); $i++) {
          if($result->items[0]->grades[$u[$i]]->grade != 0){
            if($highest_grade < $result->items[0]->grades[$u[$i]]->grade){
              $highest_graded_user = $u[$i];
              $highest_grade = $result->items[0]->grades[$u[$i]]->grade;
            }
          }
          else{
             //$i++;
          }
        }
        $content_string .= '<br/>Student : '.$allowedusers[$highest_graded_user]->username.'<br />'.'Highest score :'.$highest_grade.'<br/>';

        $assign_submission_data = $DB->get_record('assign_submission', array('userid' => $highest_graded_user, 'assignment' => $k, 'status' => 'submitted'));
        $item_id = $assign_submission_data->id;
        $href = '/moodle/blocks/assignments/assignment_file.php?userid='.$highest_graded_user.'&filearea=submission_files&itemid='.$item_id;
        $fileLink = html_writer::tag('a','Checkout solution', array('href' => $href));
        $content_string .= $fileLink;
        $content_string .= "<br /><br />";
      }
    }
    $this->content->text = $content_string;
    return $this->content;
  }
}
