<?php
defined('MOODLE_INTERNAL') || die();
$plugin->component = 'block_assignments';
$plugin->version = 2018031900;
$plugin->requires = 2017111300;
$plugin->maturity = MATURITY_STABLE;
$plugin->release = "1.0";
